#!/usr/bin/env bash

install_kitty() {
  if [ -z "$(check_if_installed "kitty")" ]; then
    echo "Installing kitty"
    curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin

    ln -s ~/.local/kitty.app/bin/kitty ~/.local/bin/

    echo "Add kitty to menu"
    # Place the kitty.desktop file somewhere it can be found by the OS
    cp ~/.local/kitty.app/share/applications/kitty.desktop ~/.local/share/applications
    # Update the path to the kitty icon in the kitty.desktop file
    sed -i "s/Icon\=kitty/Icon\=\/home\/$USER\/.local\/kitty.app\/share\/icons\/hicolor\/256x256\/apps\/kitty.png/g" ~/.local/share/applications/kitty.desktop
  fi

  echo "Re-linking kitty"
  # link kitty config
  rm $HOME/.config/kitty/kitty.conf
  ln -s $PWD/kitty.conf $HOME/.config/kitty/kitty.conf
}

# Install zsh
install_zsh() {
  if [ -z "$(check_if_installed "zsh")" ]; then
    echo "Installing zsh"
    sudo apt update
    sudo apt install zsh
    chsh -s $(which zsh)

    # install oh-my-zsh
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
  fi

  echo "Install spaceship"
  git clone https://github.com/denysdovhan/spaceship-prompt.git "$ZSH_CUSTOM/themes/spaceship-prompt"
  ln -s "$ZSH_CUSTOM/themes/spaceship-prompt/spaceship.zsh-theme" "$ZSH_CUSTOM/themes/spaceship.zsh-theme"

  echo "Install zsh-completions"
  git clone https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:=~/.oh-my-zsh/custom}/plugins/zsh-completions

  echo "Install syntax highlighting"
  git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

  echo "Re-linking zsh"
  # link zhs config
  rm $HOME/.zshrc
  ln -s $PWD/.zshrc $HOME/.zshrc
}

install_xsel(){
    echo "Install xsel"
    sudo apt install xsel
}

install_tmux(){

    echo "Installing tmux"
    sudo apt update
    sudo apt install tmux

    echo "Linking tmux"
    rm $HOME/.tmux.conf
    ln -s $PWD/.tmux.conf $HOME/.tmux.conf

    echo "Install powerline fonts"
    sudo apt install fonts-powerline

    # install tmux plugin maganer
    git clone https://github.com/tmux-plugins/tpm $HOME/.tmux/plugins/tpm
    $HOME/.tmux/plugins/tpm/bin/install_plugins
  else

    echo "Re-linking tmux"
    rm $HOME/.tmux.conf
    ln -s $PWD/.tmux.conf $HOME/.tmux.conf
    $HOME/.tmux/plugins/tpm/bin/update_plugins all
  fi
}

# Install nvim
install_nvim () {
    echo "Installing nvim"
    sudo apt install nvim
    curl -fLo $HOME/.local/share/nvim/site/autoload/plug.vim --create-dirs \
      https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

  echo "Coping nvim settings"
  rm $HOME/.config/nvim
  ln -s $PWD/.config/nvim $HOME/.config/nvim

  echo "Link plugins"
  rm $HOME/.nvim.plugins
  ln -s $PWD/.nvim.plugins $HOME/.nvim.plugins
  echo "Updating nvim"
  if [ -z "$INSTALLED" ]; then
    nvim -c ':PlugInstall | qa'
  else
    nvim -c ':PlugUpdate | qa'
  fi
}

  install_tmux
  install_zsh
  install_fzf
  install_xsel
  install_kitty
  install_nvim
